﻿using System;

namespace ConsoleApp5
{
    class Program
    {
        static void Main(string[] args)
        {
            int myNum1 = PlusMethod(8, 5);
            int myNum4 = PlusMethod(8, 5,3.3);
            double myNum2 = PlusMethod(4.3, 6.26);
            Console.WriteLine("Int: " + myNum1);
            Console.WriteLine("Double: " + myNum2);
            Person myObj = new Person();
            myObj.Name = "siva";
            Console.WriteLine(myObj.Name);
            Person per2 = new Person();
            per2.age = "2";
            Console.WriteLine(per2.age);
            baseclass bas = new baseclass();
            derivedclass des = new derivedclass();
            baseclass bas2 = new derivedclass();
            neutral ne = new neutral();
            ne.samemethod();
            bas.samemethod();
            des.samemethod();
            bas2.samemethod();
            abstractbase nor = new normalclass();
            nor.absfunction();
            nor.sleep();
            try
            {
                int[] myNumbers = { 1, 2, 3 };
                Console.WriteLine(myNumbers[10]);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
        class normalclass : abstractbase
        {
            public override void absfunction()
            {
                Console.WriteLine("weeeeeeeee");
            }
            public void exe()
            {
                
            }
        }
        abstract class abstractbase
        {
            public abstract void absfunction();
            public void sleep()
            {
                Console.WriteLine("Zzz");
            }
        }

        class baseclass
        {
            public virtual void samemethod()
            {
                Console.WriteLine("i'm in base class");
            }
        }
        class derivedclass:baseclass
        {
            public override void samemethod()
            {
                Console.WriteLine("derived one");
            }
        }
        class neutral:baseclass
        {
            public new  void samemethod()
            {
                Console.WriteLine("new one");
            }
        }
        class Person
        {
            private string name; 
            public string Name   
            {
                get { return name; }
                set { name = value; }
            }
            public string age  
            { get; set; }
        }
        static int PlusMethod(int x, int y)
        {
            return x + y;
        }
        static int PlusMethod(int x, int y,double z)
        {
            return x + y;
        }
        static double PlusMethod(double x, double y)
        {
            return x + y;
        }
    }
}
